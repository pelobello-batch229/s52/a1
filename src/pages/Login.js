import {Form, Button} from 'react-bootstrap';
import {useState, useEffect} from 'react';




export default function Login() {

	
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');


	const[isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password)


	useEffect(() => {
		
		if((email !== '' && password !== '')){
			setIsActive(true)
		}else{
			setIsActive(false)
		}
	})



	function userLogin(event) {
	
		event.preventDefault();

		setEmail('');
		setPassword('');
		

		alert("You are now logged in");
	}

	return(
		    <Form onSubmit={event=> userLogin(event)} >
		    	<h2>Login</h2>
		      <Form.Group className="mb-3" controlid="userEmail">
		        <Form.Label>Email</Form.Label>
		        <Form.Control type="email" placeholder="Enter email"  value={email} onChange={event => setEmail(event.target.value)} required/>
		        <Form.Text className="text-muted">
		          We'll never share your email with anyone else.
		        </Form.Text>
		      </Form.Group>

		      <Form.Group className="mb-3" controlid="Password">
		        <Form.Label>Password</Form.Label>
		        <Form.Control type="password" placeholder="Password" value={password} onChange={event => setPassword(event.target.value)} required/>
		      </Form.Group>

		     {
		     	(isActive)?	
		     	<Button variant="success" type="submit" controlid="submitBtn" >Login</Button>	
		     	: 
		     	<Button variant="success" type="submit" controlid="submitBtn" disabled>Login</Button>	
		     }
		      

		    </Form>
		)
}